ARG CI_REGISTRY_IMAGE

FROM ubuntu:focal

WORKDIR /OrangeFox

ENV LC_ALL="C"
ENV TZ="Etc/UTC"
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
    build-essential \
    wget \
    zip \
    curl \
    libstdc++6 \
    git \
    python3 \
    python-is-python3 \
    gcc \
    clang \
    libssl-dev \
    rsync \
    flex \
    bison \
    aria2 
    
RUN curl --create-dirs -L -o /usr/local/bin/repo -O -L https://storage.googleapis.com/git-repo-downloads/repo
RUN chmod a+rx /usr/local/bin/repo
RUN git config --global user.name "CI"
RUN git config --global user.email "CI@CI.CI"